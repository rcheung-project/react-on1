import Home from "./pages/home/Home";
import Act from "./pages/holiday/Act";
import Nsw from "./pages/holiday/Nsw";
import Qld from "./pages/holiday/Qld";
import Vic from "./pages/holiday/Vic";
import "./style/dark.scss";
import { useContext} from "react";
import { DarkModeContext} from "./context/darkModeContext";
import { BrowserRouter, Route, Routes } from "react-router-dom";

// create route to the page
function App() {
  const {darkMode} = useContext(DarkModeContext)

  return (
    <div className={darkMode ? "app dark" : "app"}>
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home/>} />
      <Route path="act" element={<Act/>}/>
      <Route path="nsw" element={<Nsw/>}/>
      <Route path="qld" element={<Qld/>}/>
      <Route path="vic" element={<Vic/>}/>
    </Routes>
  </BrowserRouter>
    </div>
  );
}

export default App;
