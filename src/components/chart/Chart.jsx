import "./chart.scss"
import { AreaChart, Area, XAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';


const data = [
  { when: 2012, Total: 22.73},
  { when: 2013, Total: 23.13},
  { when: 2014, Total: 23.48},
  { when: 2015, Total: 23.82},
  { when: 2016, Total: 24.19},
  { when: 2017, Total: 24.59},
  { when: 2018, Total: 24.97},
  { when: 2019, Total: 25.34},
  { when: 2020, Total: 25.66},
  { when: 2021, Total: 25.69},
  { when: 2022, Total: 25.98},
];

const Chart = ({aspect , title}) => {
  return (
    <div className='chart'>
      <div className="title">{title}</div>
       <ResponsiveContainer width="100%" aspect={aspect}>
            <AreaChart width={730} height={250} data={data}
        margin={{ top: 10, right: 30, left: 10, bottom: 0 }}>
        <defs>
          <linearGradient id="total" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
          </linearGradient>
        </defs>
        <XAxis dataKey="when" stroke="gray" />
        <CartesianGrid strokeDasharray="3 3" className="chartGrid" />
        <Tooltip />
        <Area type="monotone" dataKey="Total" stroke="#8884d8" fillOpacity={1} fill="url(#total)" />
      </AreaChart>
      </ResponsiveContainer>
    </div>
  )
}

export default Chart
