import DonutChart from "react-donut-chart";

const reactDonutChartdata = [
  {
    label: "NSW",
    value: 31.39,
    color: "#FEB019"
  },
  {
    label: "VIC",
    value: 25.47,
    color: "#775DD0"
  },
  {
    label: "QLD",
    value: 20.49,
    color: "#775DD0"
  },
  {
    label: "WA",
    value: 10.72,
    color: "#775DD0"
  },
  {
    label: "SA",
    value: 7.00,
    color: "#FEB019"
  },
  {
    label: "TAS",
    value: 2.20,
    color: "#FF4560"
  },
  {
    label: "ACT",
    value: 1.76,
    color: "#00E396"
  },
  {
    label: "NT",
    value: 0.97,
    color: "#FF4560"
  }
];

const reactDonutChartBackgroundColor = [
  "#00E396",
  "#FEB019",
  "#FF4560",
  "#775DD0"
];
const reactDonutChartInnerRadius = 0.5;
const reactDonutChartSelectedOffset = 0.04;
let reactDonutChartStrokeColor = "#FFFFFF";
const reactDonutChartOnMouseEnter = (item) => {
  let color = reactDonutChartdata.find((q) => q.label === item.label).color;
  reactDonutChartStrokeColor = color;
};

export default function Donut() {
  return (
    <div className="Donut">
      <DonutChart
        width={330}
        height={490}
        onMouseEnter={(item) => reactDonutChartOnMouseEnter(item)}
        strokeColor={reactDonutChartStrokeColor}
        data={reactDonutChartdata}
        colors={reactDonutChartBackgroundColor}
        innerRadius={reactDonutChartInnerRadius}
        selectedOffset={reactDonutChartSelectedOffset}
      />
    </div>
  );
}
