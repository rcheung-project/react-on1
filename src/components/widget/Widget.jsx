import "./widget.scss";

const Widget = ({ state, amt }) => {
  let data;

switch (state) {
    case "act":
        data = {
            title: "ACT",
            icon: "images/ACTflag.jpg"
        };
        break;
        case "nsw":
            data = {
                title: "NSW",
                icon: "images/NSWflag.jpg"
            };
        break;
        case "qld":
            data = {
                title: "QLD",
                icon: "images/QLDflag.jpg"
            };
            break;
        case "vic":
            data = {
                title: "VIC",
                icon: "images/VICflag.jpg"
            };
            break;
        default:
            break;
}

  return (
    <div className="widget">
      <div className="left">
            <span className="title">{data.title}</span>
            <span className="counter">{amt}%</span>
      </div>
      <div className="right">
            </div>
            <img src={data.icon} alt="" ></img>
      </div>
  );
}

export default Widget
