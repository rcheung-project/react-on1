import "./featured.scss"
import Donut from "../../components/donut/Donut"

const Featured = () => {
  return (
    <div className='featured'>
      <div className="top">
        <h1 className="title">Australian population by Jun 2022</h1><br></br>
         <p className="title">25.98 million</p>   
      </div>
      <div className="bottom">
      <div className="featuredChart">
          <Donut/>
          </div>
      </div>
    </div>
  )
}

export default Featured
