import "./navbar.scss"
import { format } from "date-fns"
import LanguageOutlinedIcon from "@mui/icons-material/LanguageOutlined";
import DarkModeOutlined from "@mui/icons-material/DarkModeOutlined";
import { useContext} from "react";
import { DarkModeContext} from "../../context/darkModeContext";



const Navbar = () => {

  const {dispatch} = useContext(DarkModeContext)

  return (
    <div className="navbar">
      <div className="wrapper">
        <div className="tdate">
        <p>Today is {format(new Date(), 'dd-MMM-yyyy')}</p>         
        </div>
        <div className="items">
          <div className="item">
          <div className="item">
            <DarkModeOutlined className="icon" onClick={() => dispatch({type:"TOGGLE"})}/>
          </div>
            <LanguageOutlinedIcon className="icon" />
            English
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar
