import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const List = () => {

 const rows = [
  {
    date: "1-2 January (Sunday & Monday)",
    holiday: "New Year's Day / additional day",
  },  
  {
    date: "26 January (Thursday)",
    holiday: "Australia Day",
  },
  {
    date: "7 April (Friday)",
    holiday: "Good Friday",
  },
  {
    date: "8 April (Saturday)",
    holiday: "Easter Saturday",
  },
  {
    date: "9 April (Sunday)",
    holiday: "Easter Sunday",
  },
  {
    date: "10 April (Monday)",
    holiday: "Easter Monday",
  },
  {
    date: "25 April (Tueday)",
    holiday: "Anzac Day",
  },
  {
    date: "12 June (Monday)",
    holiday: "King's Birthday",
  },
  {
    date: "2 October (Monday)",
    holiday: "Labour Day",
  },
  {
    date: "25 December (Monday)",
    holiday: "Christmas Day",
  },
  {
    date: "26 December (Tueday)",
    holiday: "Boxing Day",
  },
 ];


  return (
    <TableContainer component={Paper} className="table">
    <Table sx={{ minWidth: 650 }} aria-label="simple table">
      <TableHead>
        <TableRow>
          <TableCell className="tableCell">Date in 2023</TableCell>
          <TableCell className="tableCell">holiday</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row) => (
          <TableRow>
            <TableCell className="tableCell">{row.date}</TableCell>
            <TableCell className="tableCell">{row.holiday}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </TableContainer>
  )
}

export default List
