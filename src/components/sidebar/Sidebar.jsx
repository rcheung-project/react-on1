import "./sidebar.scss"
import DashboardIcon from '@mui/icons-material/Dashboard';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import {Link} from "react-router-dom";

const Sidebar = () => {

  return (
    <div className="sidebar"> 
    <div className="top">
      <Link to="/" style={{textDecoration: "none"}}>

     <span className="logo">Dashboard</span>
     </Link>
    </div>
    <hr />
    <div className="center">
        <ul>
           <p className="title">Main</p>
           <Link to="/" style={{textDecoration: "none"}}>
          <li>
            <DashboardIcon className="icon" />
              <span>Population</span>
          </li>
            </Link>
            <p className="title">public holidays</p>
            <Link to="/act" style={{textDecoration: "none"}}>
            <li>
              <CalendarMonthIcon className="icon" />
            <span>ACT</span>
            </li>
            </Link>
            <Link to="/nsw" style={{textDecoration: "none"}}>
            <li>
              <CalendarMonthIcon className="icon" />
            <span>NSW</span>
            </li>
            </Link>
            <Link to="/qld" style={{textDecoration: "none"}}>
            <li>
              <CalendarMonthIcon className="icon" />
            <span>QLD</span>
            </li>
            </Link>
            <Link to="/vic" style={{textDecoration: "none"}}>       
            <li>
              <CalendarMonthIcon className="icon" />
            <span>VIC</span>
            </li>
          </Link>
        </ul>
       </div>
    </div>
 )
}


export default Sidebar
