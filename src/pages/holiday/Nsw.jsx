import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import Tablensw from "../../components/table/Tablensw"


const Act = () => {
  return (
    <div className="home">
      <Sidebar/>
      <div className="homeContainer">
      <Navbar/>
      <div className="listContainer">
        <div className="listTitle">NSW public holidays</div>
          <Tablensw/>
        </div>
      </div>
    </div>
  )
}

export default Act