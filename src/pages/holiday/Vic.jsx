import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import Tablevic from "../../components/table/Tablevic"


const Act = () => {
  return (
    <div className="home">
      <Sidebar/>
      <div className="homeContainer">
      <Navbar/>
      <div className="listContainer">
        <div className="listTitle">VIC public holidays</div>
          <Tablevic/>
        </div>
      </div>
    </div>
  )
}

export default Act