import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import Tableact from "../../components/table/Tableact"


const Act = () => {
  return (
    <div className="home">
      <Sidebar/>
      <div className="homeContainer">
      <Navbar/>
      <div className="listContainer">
        <div className="listTitle">ACT public holidays</div>
          <Tableact/>
        </div>
      </div>
    </div>
  )
}

export default Act