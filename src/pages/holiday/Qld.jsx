import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import Tableqld from "../../components/table/Tableqld"


const Act = () => {
  return (
    <div className="home">
      <Sidebar/>
      <div className="homeContainer">
      <Navbar/>
      <div className="listContainer">
        <div className="listTitle">QLD public holidays</div>
          <Tableqld/>
        </div>
      </div>
    </div>
  )
}

export default Act