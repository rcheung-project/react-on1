import Navbar from "../../components/navbar/Navbar"
import Sidebar from "../../components/sidebar/Sidebar"
import Widget from "../../components/widget/Widget"
import Featured from "../../components/featured/Featured"
import Chart from "../../components/chart/Chart"
import "./home.scss"


const Home = () => {
  return (
    <div className="home">
      <Sidebar/>
      <div className="homeContainer">
      <Navbar/>
      <div className="widgets">
        <Widget state="act" amt="1.76"/>
        <Widget state="nsw" amt="31.39"/>
        <Widget state="qld" amt="20.49"/>
        <Widget state="vic" amt="25.47"/>
      </div>
      <div className="charts">
        <Featured/>
        <Chart title="Population (total = million)" aspect={2/1}/>
      </div>
      <div className="listContainer">
        <div className="listTitle">Data sources from ABS and worlddata.info</div>
      </div>
      </div>
    </div>
  )
}

export default Home